import os
import sys
import json
import numpy as np
import torch
from torch import nn
from torch import optim
from torch.optim import lr_scheduler

from opts import parse_opts
from model import generate_model
from mean import get_mean, get_std
from spatial_transforms import *
from temporal_transforms import *
from target_transforms import ClassLabel, VideoID
from target_transforms import Compose as TargetCompose
from dataset import get_training_set, get_validation_set, get_test_set
from utils import *
from train import train_epoch
from validation import val_epoch
import test

import torch.nn as nn
import torch.onnx
import math
import cv2

import argparse
import imutils
import sys

if __name__ == '__main__':
    opt = parse_opts()
    if opt.root_path != '':
        opt.video_path = os.path.join(opt.root_path, opt.video_path)
        opt.annotation_path = os.path.join(opt.root_path, opt.annotation_path)
        opt.result_path = os.path.join(opt.root_path, opt.result_path)
        if not os.path.exists(opt.result_path):
            os.makedirs(opt.result_path)
        if opt.resume_path:
            opt.resume_path = os.path.join(opt.root_path, opt.resume_path)
        if opt.pretrain_path:
            opt.pretrain_path = os.path.join(opt.root_path, opt.pretrain_path)
    opt.scales = [opt.initial_scale]
    for i in range(1, opt.n_scales):
        opt.scales.append(opt.scales[-1] * opt.scale_step)
    opt.arch = '{}'.format(opt.model)
    opt.mean = get_mean(opt.norm_value, dataset=opt.mean_dataset)
    opt.std = get_std(opt.norm_value)
    opt.store_name = '_'.join([opt.dataset, opt.model, str(opt.width_mult) + 'x',
                               opt.modality, str(opt.sample_duration)])
    print(opt)
    with open(os.path.join(opt.result_path, 'opts.json'), 'w') as opt_file:
        json.dump(vars(opt), opt_file)

    torch.manual_seed(opt.manual_seed)

    model, parameters = generate_model(opt)
    print(model)


    PATH='/content/kinetics_resnet_0.5x_RGB_16_best.pth'
    checkpoint=torch.load(PATH)
    model.load_state_dict(checkpoint['state_dict'])
    model.eval()
    #print(checkpoint.keys())


    classes=['taking','']
    SAMPLE_DURATION = 16
    SAMPLE_SIZE = 112

    print("[INFO] accessing video stream...")
    path_to_video='/content/video.mp4'
    vs = cv2.VideoCapture(path_to_video if path_to_video else 0)
number=0
while True:
  # initialize the batch of frames that will be passed through the
  # model
  frames = []

  # loop over the number of required sample frames
  for i in range(0, SAMPLE_DURATION):
    # read a frame from the video stream
    (grabbed, frame) = vs.read()

    # if the frame was not grabbed then we've reached the end of
    # the video stream so exit the script
    if not grabbed:
      print("[INFO] no frame read from stream - exiting")
      sys.exit(0)

    # otherwise, the frame was read so resize it and add it to
    # our frames list
    frame = imutils.resize(frame, width=400)
    frames.append(frame)
    
  print('frames 1',len(frames),frame.shape)
  # now that our frames array is filled we can construct our blob
  blob = cv2.dnn.blobFromImages(frames, 1.0,
    (SAMPLE_SIZE, SAMPLE_SIZE), (114.7748, 107.7354, 99.4750),
    swapRB=True, crop=True)
  blob = np.transpose(blob, (1, 0, 2, 3))
  blob = np.expand_dims(blob, axis=0)
  print(len(blob),blob[0].shape,blob.shape,type(blob))
  # pass the blob through the network to obtain our human activity
  # recognition predictions
  outputs=model(torch.from_numpy(blob))
  #net.setInput(blob)
  #outputs = net.forward()
  print(outputs)
  label=classes[torch.argmax(outputs).item()]
  #label = classes[np.argmax(outputs)]
  print(label)

  # loop over our frames
  for frame in frames:
    # draw the predicted activity on the frame
    cv2.rectangle(frame, (0, 0), (300, 40), (0, 0, 0), -1)
    cv2.putText(frame, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX,
      0.8, (255, 255, 255), 2)

    # display the frame to our screen
    name = "%d.jpg"%number
    cv2.imwrite('/content/frames/'+name, frame)
    number+=1
    print(number)
    #cv2.imshow("Activity Recognition", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
      break

import cv2
import numpy as np
import glob
import os

img_array = []
for filename in sorted(glob.glob('/content/frames/*.jpg'), key=os.path.getmtime):
    #print(filename)
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width,height)
    img_array.append(img)
 
 
out = cv2.VideoWriter('/content/test.mp4',cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
 
for i in range(len(img_array)):
    out.write(img_array[i])
out.release()
