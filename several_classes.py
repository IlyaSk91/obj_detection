import time
import pandas as pd

# функция выбора классов
def separate_classes(path,classes):
  df=pd.read_csv(path)
  classes_videos=pd.DataFrame()
  for i in range(df.shape[0]):
    if df.iloc[i,:]['label'] in classes:
      classes_videos=classes_videos.append(df.iloc[i,:])
    else:
      continue
  return classes_videos
# создание csv с выбранными метками
train=separate_classes(path='/ml/Ilya_projects/kinetics400/train.csv',classes=set(['picking friut','push up']))
train=train.reset_index(drop=True)
train.to_csv('/ml/Ilya_projects/train_another_class.csv')
valid=separate_classes(path='/ml/Ilya_projects/kinetics400/validate.csv',classes=set(['picking friut','push up']))
valid=valid.reset_index(drop=True)
valid.to_csv('/ml/Ilya_projects/validate_another_class.csv')

import youtube_dl
import re

# ф-ия скачивания видео
def download_video(video):
  result=re.search(r'\d+',youtube_dl.YoutubeDL().extract_info('https://www.youtube.com/watch?v='+video, download=False)['format_id'])
  ydl_opts = {'outtmpl': video+'.mp4','format':result.group(0),} # имя файла video, с разрешением 360p.
  #with youtube_dl.YoutubeDL(ydl_opts) as ydl:
  youtube_dl.YoutubeDL(ydl_opts).download(['https://www.youtube.com/watch?v='+video])
  print('скачано')
  return 1

import os
os.mkdir ('/ml/Ilya_projects/another_class/')
os.mkdir ('/ml/Ilya_projects/another_class/train/')
os.chdir ('/ml/Ilya_projects/another_class/train/')

import os
for label in ['picking friut','push up']:
  os.mkdir('/ml/Ilya_projects/another_class/train/'+label)


with open('/ml/Ilya_projects/file_another_class.txt', 'w') as f:  # запись файлов, которые не скачались
  for i in range(train.shape[0]):
    if train.iloc[i]['label']=='picking friut':  # 1 класс
      os.chdir('/ml/Ilya_projects/another_class/train/picking friut/')
      result = None
      k=0
      # пробуем скачать 5 раз,если нет, то бросаем и переходим к скачиванию следующего
      while result is None:
        try:
          result=download_video(train.iloc[i]['youtube_id'])
          new_name = train.iloc[i]['youtube_id'] + '_' + (str(0) * (6 - len(str(int(train.iloc[i]['time_start'])))) + str(int(train.iloc[i]['time_start'])))+ '_' + (str(0) * (6 - len(str(int(train.iloc[i]['time_end'])))) + str(int(train.iloc[i]['time_end']))) + '.mp4'
          os.rename(train.iloc[i]['youtube_id'] + '.mp4', new_name)
        except:
          pass
        k = k + 1
        #print('train', train.iloc[i]['youtube_id'])
        #f.write('train' + ' ' + train.iloc[i]['youtube_id'])
        if k==5:
          f.write('train' + ' ' + str(i) + ' ' + train.iloc[i]['youtube_id'] + '\n')
          break

    else:
      os.chdir('/ml/Ilya_projects/another_class/train/push up/') # 2 класс
      result = None
      k=0
      # пробуем скачать 5 раз,если нет, то бросаем и переходим к скачиванию следующего
      while result is None:
        try:
          result=download_video(train.iloc[i]['youtube_id'])
          new_name = train.iloc[i]['youtube_id'] + '_' + (str(0) * (6 - len(str(int(train.iloc[i]['time_start'])))) + str(int(train.iloc[i]['time_start']))) + '_' + (str(0) * (6 - len(str(int(train.iloc[i]['time_end'])))) + str(int(train.iloc[i]['time_end']))) + '.mp4'
          os.rename(train.iloc[i]['youtube_id'] + '.mp4', new_name)
        except:
          pass
        k = k + 1
        if k == 5:
          #print('train', train.iloc[i]['youtube_id'])
          #f.write('train' + ' ' + train.iloc[i]['youtube_id'])
          f.write('train' + ' ' + str(i) + ' ' + train.iloc[i]['youtube_id'] + '\n')
          break
    # таймаут на каждые 20 видео
    if i%20==0:
      print('timeout')
      time.sleep(60)

  os.chdir('/ml/Ilya_projects/')

  # тоже самое и для валидации
  for i in range(valid.shape[0]):
    if valid.iloc[i]['label']=='picking fruit':
      os.chdir('/ml/Ilya_projects/another_class/train/picking fruit/')
      result = None
      k=0
      while result is None:
        try:
          result=download_video(valid.iloc[i]['youtube_id'])
          new_name = valid.iloc[i]['youtube_id'] + '_' + (str(0) * (6 - len(str(int(valid.iloc[i]['time_start'])))) + str(int(valid.iloc[i]['time_start']))) + '_' + (str(0) * (6 - len(str(int(valid.iloc[i]['time_end'])))) + str(int(valid.iloc[i]['time_end']))) + '.mp4'
          os.rename(valid.iloc[i]['youtube_id'] + '.mp4', new_name)
        except:
          pass
        k = k + 1
        if k == 5:
          #print('validation', valid.iloc[i]['youtube_id'])
          #f.write('validation' + ' ' + valid.iloc[i]['youtube_id'])
          f.write('validation' + ' ' + str(i) + ' ' + valid.iloc[i]['youtube_id'] + '\n')
          break

    else:
      os.chdir('/ml/Ilya_projects/another_class/train/push up/')
      result = None
      k=0
      while result is None:
        try:
          result=download_video(valid.iloc[i]['youtube_id'])
          new_name = valid.iloc[i]['youtube_id'] + '_' + (str(0) * (6 - len(str(int(valid.iloc[i]['time_start'])))) + str(int(valid.iloc[i]['time_start']))) + '_' + (str(0) * (6 - len(str(int(valid.iloc[i]['time_end'])))) + str(int(valid.iloc[i]['time_end']))) + '.mp4'
          os.rename(valid.iloc[i]['youtube_id'] + '.mp4', new_name)
        except:
          pass
        k = k + 1
        if k == 5:
          #print('validation', valid.iloc[i]['youtube_id'])
          #f.write('validation' + ' ' + valid.iloc[i]['youtube_id'])
          f.write('validation' + ' ' + str(i) + ' ' + valid.iloc[i]['youtube_id'] + '\n')
          break

    if i%20==0:
      print('timeout')
      time.sleep(60)

f.close()
os.chdir('/ml/Ilya_projects/')
